/* main.cpp : Defines the entry point for the console application.
   It is the console executable that makes use of the BullCowGame.
   This acts as the view in a MWC Pattern and is responsible for all user interaction.
   For game logic see the FBullCowGame class.
*/
#pragma once
#include "stdafx.h"
#include <iostream>
#include <string>
#include "FBullCowGame.h"

using FText = std::string;
using int32 = int;

void PrintIntro();
void PlayGame();
FText GetValidUserGuess();
void PrintUserGuess(FText Guess);
bool AskToPlayAgain();


FBullCowGame BCGame = FBullCowGame(); // instantiate/create an instance of a new game

int main()
{
    // set console to german 
    std::locale::global(std::locale("German_germany"));

    bool bPlayAgain = false;
    do 
    {
        PrintIntro();
        BCGame.Reset();	// Resets the game to default values
        PlayGame();
        bPlayAgain = AskToPlayAgain();
        std::cout << std::endl;
    } 
    while (bPlayAgain == true);	// loop as long as player wants to play
   
    return 0; // exit application
}


// introduce the game
void PrintIntro()
{
    std::cout << "Willkommen bei Bulls & Cows, ein lustiges Spiel mit W�rtern :D\n";
    std::cout << "Kannst du das Isogramm, an das ich denke, herausfinden?\n";
    std::cout << "Achte auf Gro�- und Kleinschreibung!\n";
	std::cout << "Bulls sind dabei Buchstaben, die an der gleichen Stelle im gesuchten Wort stehen. \nCows sind Buchstaben, die enthalten sind aber nicht an deiner eingegeben Stelle stehen.\n";
    std::cout << std::endl;
    return;
}


void PlayGame()
{   
	int32 MaxTries = BCGame.GetMaxTries();

	// loop asking for guesses while the game is NOT won
	// and there are still tries remaining
    
	while(BCGame.GetIsGameWon() == false && BCGame.GetCurrentTry() <= BCGame.GetMaxTries())
    {
        FText Guess = GetValidUserGuess(); 
		
        // submit valid guess to game and receives counts
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);
		
        // print number of bulls and cows
		std::cout << "Bulls: " << BullCowCount.Bulls;
		std::cout << " Cows: " << BullCowCount.Cows << std::endl;
		std::cout << std::endl;	
		BCGame.ResetBullCowCount();
    }
	BCGame.PrintGameSummary();	// Print the winning or losing text
	return;
}

// loop till the user gives a valid guess
FText GetValidUserGuess() 
{
	FText Guess = "";
	EGuessStatus Status = EGuessStatus::Invalid;
	do {
		// Setup for asking player
		int32 CurrentTry = BCGame.GetCurrentTry();
		int32 MaxTries = BCGame.GetMaxTries();
		if (CurrentTry == 1) {
			std::cout << "Gib deine Vermutung des Wortes ein!\n";
			std::cout << "Versuch " << CurrentTry << " von " << MaxTries << ": ";
		}
		else
		{
			std::cout << "Gib einen neuen Vorschlag ein!\n";
			std::cout << "Versuch " << CurrentTry << " von " << MaxTries << ": ";
		}
		// Ask player for guess
		std::getline(std::cin, Guess);
		Status = BCGame.bIsGuessValid(Guess); // is the guess valid? if not give out error message
		switch (Status)
		{
		case EGuessStatus::Wrong_Length:
			std::cout << "Bitte gib ein Wort mit " << BCGame.GetHiddenWordLength() << " Buchstaben ein D: \n\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "Bitte gib ein Wort mit nicht wiederholenden Buchstaben (= Isogramm) ein D: \n\n";
			break;
		default:
			break; // assuming the guess is valid
		}
	} while (Status != EGuessStatus::OK); // keep looping till we got no errors
	
	return Guess;
}

void PrintUserGuess(FText Guess) 
{
    std::cout << "Du hast " << Guess << " geraten, richtig?\n";
    std::cout << std::endl;
    return;
}

bool AskToPlayAgain()
{
    std::cout << "Willst du nochmal mit dem gleichen unbekannten Wort spielen? [j/n]";
    FText Response = "";
    std::getline(std::cin, Response);
    std::cout << std::endl;
    return (Response[0] == 'j' || Response[0] == 'J');
}

/* The game interface/logic. No code or interactions of user. */

#pragma once
#include <string>
#include <map>
#define TMap std::map

// to make syntax Unreal friendly
using FString = std::string;
using int32 = int;

// all values intialised to zero
struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0; 
};

enum class EGuessStatus
{
	Invalid,
	OK,
	Not_Isogram,
	Wrong_Length
};

class FBullCowGame 
{
	// Cows are letters that are in the word but at the wrong position
	// Bulls are letters that are in the word but at the right position

public:
	FBullCowGame();									// constructor

	// Getter
	int32 GetMaxTries() const;						// How many tries the player has
	int32 GetCurrentTry() const;					// what try is the player on
	FString GetHiddenWord() const;					// what is the hidden word
	int32 GetHiddenWordLength() const;				// get the hidden word length
	FBullCowCount GetBullCowCount() const;
	bool GetIsGameWon() const;
	TMap<int32, FString> GetIsogramArray() const;

	// Setter
	void SetCurrentTry(int32);						// set how often the player tried
	void SetMaxTries(char);						// how many tries the player wants
	void SetHiddenWord(FString);					// set the hidden word
	void SetIsGameWon(bool);
	void SetHiddenWordLength(int32);

		void SetThreeLetterIsos();
		void SetFourLetterIsos();
		void SetFiveLetterIsos();
		void SetSixLetterIsos();
		void SetSevenLetterIsos();
		void SetIsogramArray();
		void EmptyIsogramArray();
	void SetHiddenWord();
	void ResetBullCowCount();
	// TODO make it get a hidden word from a dictionary at the start

	void Reset();									
	
	void PrintGameSummary();

	EGuessStatus bIsGuessValid(FString) const;		// Gets in a string and gives out a Enum Status
													// is the guess typed in even valid?

	bool WhenToShowHint();							// Condition on when to show hint

	FBullCowCount SubmitValidGuess(FString);

	
// ignore this for now and focus on the interface above
private:
	// initialisaion at constructor
	int32 MyCurrentTry;
	int32 MyMaxTries;
	FString MyHiddenWord;
	int32 MyHiddenWordLength;
	FString MyCurrentGuess;
	FBullCowCount BullCowCount;
	bool bIsGameWon;

	int32 PlayerWhatWordLength();
	int32 InputToValidInteger(FString HowLongWordText, FString WrongInputText);

	char PlayerWhatDifficulty();						// returns player input for what difficulty he wants
	char FBullCowGame::InputToValidDifficulty(FString HowManyTriesText, FString WrongInputText);
	// Input of preset text 

	bool IsIsogramm(FString) const;				// is every letter unique in the word?
	int32 RandomNumberWord();
	TMap<int32, FString> IsogramArray;	// all isograms with 3 letters
};

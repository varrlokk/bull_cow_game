/*This is the main game class that handles all influences of the player and gives a response*/
#pragma once
#include "stdafx.h"
#include "FBullCowGame.h"
#include <string>
#include <random>
#include <sstream>
#include <iostream>
#include <iomanip>

int32 StringInputToNumber(FString, FString, int32);

FBullCowGame::FBullCowGame()
{
	SetCurrentTry(0);
	const FString HIDDEN_WORD = "Vorteil";	
	SetHiddenWord(HIDDEN_WORD);
}

// Getter
int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }
FString FBullCowGame::GetHiddenWord() const { return MyHiddenWord; }
int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWordLength; }
FBullCowCount FBullCowGame::GetBullCowCount() const { return BullCowCount; }
int32 FBullCowGame::GetMaxTries() const { return MyMaxTries; }
bool FBullCowGame::GetIsGameWon() const { return bIsGameWon; }
TMap<int32, FString> FBullCowGame::GetIsogramArray() const { return IsogramArray; }



// Setter
void FBullCowGame::SetMaxTries(char difficulty) // Decide the maximum tries by players setup for difficulty
{ 
	if (difficulty == 'e') 
	{
		TMap<char, int32> EasyMaxTries{ { 3,7 },{ 4,10 },{ 5,16 },{ 6,20 },{ 7,25 } };
		MyMaxTries = EasyMaxTries[GetHiddenWordLength()];
	}
	else if (difficulty == 'n')
	{
		TMap<char, int32> NormalMaxTries{ { 3,4 },{ 4,7 },{ 5,10 },{ 6,16 },{ 7,20 } };
		MyMaxTries = NormalMaxTries[GetHiddenWordLength()];
	}
	else if (difficulty == 'h')
	{
		TMap<char, int32> HardMaxTries{ { 3,2 },{ 4,4 },{ 5,7 },{ 6,12 },{ 7,16 } };
		MyMaxTries = HardMaxTries[GetHiddenWordLength()];
	}
}
void FBullCowGame::SetCurrentTry(int32 number) { MyCurrentTry = number; }
void FBullCowGame::SetHiddenWord(FString word) { MyHiddenWord = word; }
void FBullCowGame::SetIsGameWon(bool bGameWon) { bIsGameWon = bGameWon; }
void FBullCowGame::SetHiddenWordLength(int32 number) { MyHiddenWordLength = number; }

	void FBullCowGame::SetThreeLetterIsos()
	{
		IsogramArray = { {1,"Abi"}, {2,"Abo"}, {3,"Amt"}, {4,"Eis"} };
	}
	void FBullCowGame::SetFourLetterIsos()
	{
		IsogramArray = { {1,"Zahn"}, {2,"Igel"},{3,"Nase"}, {4,"Hund"},{5,"Ufer"} };
	}
	void FBullCowGame::SetFiveLetterIsos()
	{
		IsogramArray = { { 1,"Falke" },{ 2,"Hitze" },{ 3,"Katze" },{ 4,"Liste" },{ 5,"Sonde" } };
	}
	void FBullCowGame::SetSixLetterIsos()
	{
		IsogramArray = { { 1,"Bezirk" },{ 2,"Abitur" },{ 3,"Winter" },{ 4,"Cursor" },{ 5,"Dackel" } };
	}
	void FBullCowGame::SetSevenLetterIsos()
	{
		IsogramArray = { { 1,"Badeort" },{ 2,"Abgrund" },{ 3,"Kaliber" },{ 4,"Panther" },{ 5,"Landgut" } };
	}
	void FBullCowGame::SetIsogramArray()
	{
		switch (GetHiddenWordLength())
		{
		case 3:
			SetThreeLetterIsos();
			break;
		case 4:
			SetFourLetterIsos();
			break;
		case 5:
			SetFiveLetterIsos();
			break;
		case 6:
			SetSixLetterIsos();
		case 7:
			SetSevenLetterIsos();
		default:
			break;
		}	
	}
	void FBullCowGame::EmptyIsogramArray()
	{
		IsogramArray.clear();
	}
void FBullCowGame::SetHiddenWord()
{
	MyHiddenWord = IsogramArray[RandomNumberWord()];
}

// Functions
void FBullCowGame::ResetBullCowCount()
{
	BullCowCount.Bulls = 0;
	BullCowCount.Cows = 0;
}

void FBullCowGame::Reset() // function for resetting game important variables
{
	SetCurrentTry(1);
	SetHiddenWordLength(PlayerWhatWordLength());
	SetMaxTries(PlayerWhatDifficulty());
	SetIsogramArray();
	SetHiddenWord();
	ResetBullCowCount();
	SetIsGameWon(false);
	return;
}

// Ask the player what word length he wants and setup text and errortext
int32 FBullCowGame::PlayerWhatWordLength()
{
	int32 PlayerInputInt;	// the players input in integer
	FString WhatDifficultyText = "Wie lang soll dein Wort sein? [3 - 7] ";
	FString WrongInputText = "Bitte gib eine Zahl zwischen 3 und 7 ein \\o/ \n\n";
	PlayerInputInt = InputToValidInteger(WhatDifficultyText, WrongInputText);	// call if valid input with the two predefined texts
	return PlayerInputInt;
}

int32 FBullCowGame::InputToValidInteger(FString HowLongWordText, FString WrongInputText)	// is the player input valid meaning a number of a predefined range
{
	int32 PlayerInputNumber;
	do
	{
		std::cout << HowLongWordText;
		std::cin >> PlayerInputNumber;
		std::cout << std::endl;
		std::cin.clear();
		std::cin.ignore(1);
		if (PlayerInputNumber < 3 || PlayerInputNumber > 7) { std::cout << WrongInputText; }
	} while (PlayerInputNumber < 3 || PlayerInputNumber > 7);
	return PlayerInputNumber;
}

// Ask the player what dificulty he wants and setup text and errortext
char FBullCowGame::PlayerWhatDifficulty()
{
	char PlayerInputChar;
	FString WhatDifficultyText = "Welche Schwierigkeit willst du haben? [e/n/h] ";
	FString WrongInputText = "Bitte gib e, n oder h f�r die Schwierigkeit ein \\o/ \n\n";
	PlayerInputChar = InputToValidDifficulty(WhatDifficultyText, WrongInputText);
	return PlayerInputChar;
}

// wether the Input is a defined difficulty
char FBullCowGame::InputToValidDifficulty(FString WhatDifficultyText, FString WrongInputText)
{
	char PlayerInputChar;
	FString PlayerInput;

	while (true) {
		std::cout << WhatDifficultyText;
		std::getline(std::cin, PlayerInput);
		std::cout << std::endl;

		// This code converts from string to number NOT NEEDED HERE
		// std::stringstream PlayerStream(PlayerInput);
		if (PlayerInput.length() == 1)
		{
			if (PlayerInput[0] == 'e' || PlayerInput[0] == 'n' || PlayerInput[0] == 'h') 
			{ 
				PlayerInputChar = PlayerInput[0];
				break; 
			}		
		}
		std::cout << WrongInputText;
	}
	return PlayerInputChar;
}

void FBullCowGame::PrintGameSummary()
{
	// if game is won and the number of tries is lower or equal to max tries print win
	if (GetIsGameWon() == true)
	{
		std::cout << "Gut gemacht du hast in " << GetCurrentTry()-1 << "/" << GetMaxTries() << " Versuchen richtig geraten :D \n\n";
	} 
	else 
	{
		std::cout << "Keine Versuche mehr. Leider hast du das Wort nicht erraten :/ \n\n";
	}
}

bool FBullCowGame::WhenToShowHint()
{
	return false;
}   // TODO Hint system

EGuessStatus FBullCowGame::bIsGuessValid(FString Guess) const
{
	if (!IsIsogramm(Guess) == true)		 
	{
		return EGuessStatus::Not_Isogram;
	}
	else if(Guess.length() != GetHiddenWordLength())	// if wordlength is not right
	{
		return EGuessStatus::Wrong_Length;
	}
	// other wise
	else
	{
		return EGuessStatus::OK;
	}
	
}

bool FBullCowGame::IsIsogramm(FString Guess) const
{
	// tread 0 and 1 letter words as isograms
	if (Guess.length() <= 1) { return true; }

	TMap<char, bool> LetterSeen;							// setup a new map  
	for (auto Letter : Guess)								// loop through all letters of guess
	{
		Letter = tolower(Letter);							// handle mixed case
		if (LetterSeen[Letter] == true)	{ return false; }	// if letter is in map 
		else { LetterSeen[Letter] = true; }					// add letter to map
	}
	return true;											// for special cases like /0 
}

int32 FBullCowGame::RandomNumberWord()
{
	std::mt19937 Randomizer;
	Randomizer.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist6(1, IsogramArray.size()); // TODO not right upperceiling yet
	return dist6(Randomizer);
}

// receives a VALID guess, and returns count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrentTry++;
	// loop through all letters in the hidden word
	for (int32 MHWChar = 0; MHWChar < GetHiddenWordLength(); MHWChar++) // assuming hidden word length is same as guess length
	{
		// compare letters against guess
		for (int32 GChar = 0; GChar < GetHiddenWordLength(); GChar++)
		{
			// if they match then
			if (Guess[GChar] == MyHiddenWord[MHWChar]) 
			{
				if (MHWChar == GChar) // if they are in the same place
				{
					BullCowCount.Bulls++; // increment bulls
				}
				else
				{
					BullCowCount.Cows++; // must be a cow
				}
			}
		}			
	}
	if (BullCowCount.Bulls == GetHiddenWordLength())
	{
		SetIsGameWon(true);
	}
	return BullCowCount;
}
